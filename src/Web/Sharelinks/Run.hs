-- | Entry point to the application and web routing


module Web.Sharelinks.Run where

import Web.Scotty
import qualified Lucid as H
import qualified Data.Text as T
import qualified Data.Text.Lazy.Encoding as T

import Control.Monad
import Control.Monad.Reader
import Control.Concurrent
import Control.Concurrent.STM

-- basic auth: https://ro-che.info/articles/2016-04-14-scotty-http-basic-auth
import Network.Wai.Middleware.HttpAuth
import Data.SecureMem -- for constant-time comparison

import Web.Sharelinks.DB
import Web.Sharelinks.Html
import Web.Sharelinks.Style
import Web.Sharelinks.Config

run :: IO ()
run = do
  runCfg =<< parseArgs =<< defaultDbFilePath

runCfg :: Config -> IO ()
runCfg config = do
  initializeDB `runReaderT` config
  dbQueue <- newTBQueueIO 100
  void $ forkIO $ dbWorker config dbQueue
  scotty (cfgPort config) (router config dbQueue)

router :: Config -> TBQueue Message -> ScottyM ()
router cfg dbQueue = do
  case cfgPassword cfg of
    Nothing -> pure ()
    Just password ->
      middleware $ basicAuth
        ( \u p -> pure $
          secureMemFromByteString u == cfgUsername cfg
          && secureMemFromByteString p == password
        )
        "Sharelinks"

  get "/style.css" $ do
    setHeader "Content-Type" "text/css; charset=utf-8"
    raw $ T.encodeUtf8 style

  get "/" $ do
    links <- getLinks dbQueue
    html $ H.renderText (linksHtml cfg "/tags" links)

  get (regex "/tags/(.*)") $ do
    tagsRoute <- case cfgFilterMode cfg of
      Accumulate -> param "0"
      Replace -> pure "/tags"
    requestedTags <- map T.strip . T.split ('/'==) <$> param "1"
    links <-
      fmap
        (filter $ \Link{_tags} -> all (`elem` _tags) requestedTags)
        (getLinks dbQueue)
    html $ H.renderText (linksHtml cfg tagsRoute links)

  get "/add" $ do
    html $ H.renderText $ addHtml cfg

  post "/add" $ do
    addLink dbQueue =<< parseLink
    redirect "/"

  post "/delete" $ do
    p <- param "hash"
    removeLink dbQueue p
    redirect "/"

