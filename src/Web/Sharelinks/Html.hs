-- | Html pages

{-# language OverloadedStrings #-}

module Web.Sharelinks.Html where

import Data.Foldable
import Data.List (intersperse)
import Data.Time
import Control.Monad
import qualified Lucid as H
import qualified Data.Set as S
import Data.Text (Text)

import Web.Sharelinks.Config
import Web.Sharelinks.DB

-- HTML --

type Html = H.Html ()

template :: Text -> Html -> Html
template title body =
  H.doctypehtml_ $ do
    H.head_ $ do
      H.meta_ [ H.charset_ "utf-8" ]
      H.title_ (H.toHtml $ title)
      -- H.meta_ [ H.name_ "viewport", H.content_ "width=device-width, initial-scale=0.5" ]
      H.link_ [ H.rel_ "stylesheet", H.type_ "text/css", H.href_ "/style.css"  ]
    H.body_ $ do
      H.div_ [ H.class_ "main" ] $ do
        body

--

linksHtml :: Config -> Text -> [Link] -> Html
linksHtml cfg tagsRoute links = do
  template (cfgName cfg) $ do
    H.div_ $ do
      H.h1_ [ H.class_ "header-h1" ] $ H.a_ [H.href_ "/", H.class_ "header"] (H.toHtml $ cfgName cfg)
      H.span_ [ H.class_ "header-" ] "ー"
      H.span_ [ H.class_ "new" ] $ H.a_ [H.href_ "/add", H.class_ "newlink"] $ "New"
    H.div_ [H.class_ "links"] $ for_ links (linkHtml cfg tagsRoute)
    H.div_ [H.class_ "tags"] $ do
      sequence_
        $ intersperse (H.span_ [H.class_ "comma"] ",")
        $ map (\tag -> H.a_ [H.class_ "tag", H.href_ $ tagsRoute <> "/" <> tag] (H.toHtml tag))
        $ S.toList
        $ S.unions
        $ map (S.fromList . _tags) links

linkHtml :: Config -> Text -> Link -> Html
linkHtml cfg tagsRoute l@Link{..} = do
  H.div_
    [ case cfgDisplayMode cfg of
        Grid -> H.class_ "link"
        List -> H.class_ "link-list"
    ] $ do
    H.form_
      [ H.method_ "post"
      , H.action_ "/delete"
      , H.onsubmit_ "return confirm('Are you sure?')"
      , H.class_ "delete"
      ] $ do
        H.input_ [H.type_ "hidden", H.name_ "hash", H.value_ (toMD5 l)]
        H.input_ [H.type_ "submit", H.value_ "x", H.class_ "deletebtn"]

    H.ul_ [] $ do
      H.li_ $ do
        H.h4_ $ H.toHtml _title
        H.span_ [ H.class_ "date" ] (H.toHtml $ showGregorian _date)
        (unless (null _tags) $ H.span_ [H.class_ "comma"] "-")
        sequence_
          $ intersperse (H.span_ [H.class_ "comma"] ",")
          $ map (\tag -> H.a_ [H.class_ "tag", H.href_ $ tagsRoute <> "/" <> tag] (H.toHtml tag))
          $ _tags
      H.li_ $ H.a_ [H.href_ _link] (H.toHtml _link)
      H.li_ (H.toHtml _desc)

addHtml :: Config -> Html
addHtml cfg = do
  template (cfgName cfg <> " - " <> "Add Link") $ do
    H.div_ $ do
      H.h1_ [ H.class_ "header-h1" ] $ H.a_ [H.href_ "/", H.class_ "header"] (H.toHtml $ cfgName cfg)
    H.form_ [H.class_ "add-div", H.method_ "post", H.action_ "/add"] $ do
      H.ul_ $ do
        H.li_ $ H.input_ [H.type_ "text", H.name_ "title", H.placeholder_ "Title"]
        H.li_ $ H.input_ [H.type_ "url", H.name_ "link", H.placeholder_ "Link"]
        H.li_ $ H.input_ [H.type_ "text", H.name_ "tags", H.placeholder_ "Tags separated by comma"]
        H.li_ $ H.textarea_ [H.name_ "desc", H.placeholder_ "Description"] ""
      H.input_ [H.type_ "submit", H.value_ "Add"]

