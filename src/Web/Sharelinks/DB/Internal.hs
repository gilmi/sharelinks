-- | Interact with the json db file

module Web.Sharelinks.DB.Internal where

import Control.Monad.Reader
import Data.Aeson
import Data.Functor
import Data.Time
import Data.Digest.Pure.MD5
import System.Directory
import System.FilePath
import Web.Scotty
import qualified Data.Text as T
import qualified Data.Set as S
import Control.Concurrent.STM

import Web.Sharelinks.Config
import Web.Sharelinks.DB.Types

-- Actions --

initializeDB :: MonadIO m => MonadReader Config m => m ()
initializeDB = do
  dbFilePath <- getDbFilePath
  liftIO $ createDirectoryIfMissing True (takeDirectory dbFilePath)
  dbFileExists <- liftIO $ doesFileExist dbFilePath
  unless dbFileExists $ do
    liftIO $ writeFile dbFilePath "[]"

defaultDbFilePath :: IO FilePath
defaultDbFilePath =
  getHomeDirectory <&> (<> "/.local/sharelinks/db.json")

parseLink :: ActionM Link
parseLink = do
  t <- liftIO (utctDay <$> getCurrentTime)
  Link
    <$> (T.strip <$> param "title")
    <*> pure t
    <*> (T.strip <$> param "desc")
    <*> (T.strip <$> param "link")
    <*> (processTags <$> param "tags")
  where
    processTags =
      ( S.toList
      . S.fromList
      . filter (not . T.null)
      . map (T.toLower . T.strip)
      . T.split (==',')
      )

addLink :: MonadIO m => TBQueue Message -> Link -> m ()
addLink queue link =
  liftIO $ atomically $ writeTBQueue queue (AddLink link)

removeLink :: MonadIO m => TBQueue Message -> T.Text -> m ()
removeLink queue linkhash =
  liftIO $ atomically $ writeTBQueue queue (RemoveLink linkhash)

getLinks :: MonadIO m => TBQueue Message -> m [Link]
getLinks queue = liftIO $ do
  responseVar <- newEmptyTMVarIO
  atomically $ writeTBQueue queue (GetLinks responseVar)
  atomically $ takeTMVar responseVar

-- IO Operations --

getLinksIO :: MonadIO m => MonadReader Config m => m [Link]
getLinksIO = do
  either error pure =<< liftIO . eitherDecodeFileStrict' =<< getDbFilePath

addLinkIO :: MonadIO m => MonadReader Config m => Link -> m ()
addLinkIO l = do
  dbf <- getDbFilePath
  links <- getLinksIO
  liftIO $ encodeFile dbf (l : links)

removeLinkIO :: MonadIO m => MonadReader Config m => T.Text -> m ()
removeLinkIO linkhash = do
  dbf <- getDbFilePath
  links <- filter ((/= linkhash) . toMD5) <$> getLinksIO
  liftIO $ encodeFile dbf links

getDbFilePath :: MonadReader Config m => m FilePath
getDbFilePath =
  asks cfgJson

-- Hash --

toMD5 :: Link -> T.Text
toMD5 =  T.pack . show . md5 . encode

lookupMD5IO :: MonadIO m => MonadReader Config m => T.Text -> m Link
lookupMD5IO linkhash = do
  links <- getLinksIO <&> map (\link -> (toMD5 link, link))
  maybe (error "hash not found") pure $ lookup linkhash links

-- Worker --

dbWorker :: Config -> TBQueue Message -> IO ()
dbWorker cfg queue =
  forever $ do
    atomically (readTBQueue queue) >>= \case
      AddLink link ->
        runReaderT (addLinkIO link) cfg
      RemoveLink link ->
        runReaderT (removeLinkIO link) cfg
      GetLinks responseVar -> do
        results <- runReaderT getLinksIO cfg
        atomically (putTMVar responseVar results)

